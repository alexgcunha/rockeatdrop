import { createAppContainer, createSwitchNavigator} from 'react-navigation'
import Main from './Page/Main/index'
import Box from './Page/Box/index'

const Routes = createAppContainer(
    createSwitchNavigator({
        Main,
        Box
    })
);


export default Routes;