import axios from 'axios';

const api = axios.create({
    baseURL: "https://on-beckend.herokuapp.com"
})


export default api;